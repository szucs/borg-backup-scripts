## Install instructions for setting up borg backup

See the borg backup website and documentation for more information:

  https://www.borgbackup.org/

  https://borgbackup.readthedocs.io/en/stable/index.html

## Initalize the backup repository

After installing the required packages on your computer, create a password for encrypting the backup (without brackets):

  `secret-tool store --label='Borg-backup' user [username] domain [disk-name]`

Where [username] and [disk-name] are your user name and an arbitrary name for the chosen repository, respectively. Follow the instructions. Once the password is set up you can check and modify it e.g. using the "Passwords and Keys" tool.

Note: on KDE desktop environment kwallet can be used to store the backup encryption password. In the kwallet GUI utility create *borg-backup* folder in the default wallet (kdewallet) and add entry with *disk-name* with a strong password. The backup script will use this if no relevant entry specified in the secret-tool utility.

*Important*: The borg-backup.sh script (see below) must be modifief in its line 9 to reflect the [username] and [disk-name] as given in the above command.

To initialise the backup repository, type the following (without brackets):

  `borg init --encryption=repokey-blake2 [/path/to/repo]`
  
Where the [/path/to/repo gives] e.g. the mounting location of an external disk. To check the currently mounted disks and their mount location, type

  `df -h`
  
Create the backup repository folder on the disk and give the path to the folder in the first command.

We will encrypt the backup using the [blake2](https://blake2.net/) algorithm, which allowes quicker encryption and checksum calculation.


## User backup scripts

Copy the borg-backup.sh and borg-reminder.sh scripts to the ~/bin folder. Make 
sure that this location is added to your PATH, i.e.

  `echo $PATH`
  
if the folder is not listed then add:

  `export PATH=$PATH:~/bin`

Make sure that both scripts are executable, i.e. `chmod +x borg-*.sh`

To run the backup script type borg-backup.sh in the terminal at any location. By default the script will backup your home folder. Edit borg-backup.sh around line 34 to add additional folders. 

*Importnant*: change the BORG_REPO variable in the borg-backup.sh script to the location of the previously initialised repository ([/path/to/repo]).

The script exclude files that are listed in the following file:

  `~/.config/borg/exclude-list`

Some sane defults are provided here, create the above given folder if it doesn't exists and copy the file there. Feel free to add additional folders to exclude from backing up.

The script saves the date and time of the last successful backup to

  `~/.config/borg/latest-successful-backup`
  
This is used by the backup reminder script below.

To create the first backup run the borg-backup.sh script.

## Systemd user services for notifications

Note that this systemd rule requires the borg-reminder.sh script in the ~/bin directory.
The rule consits of a service and a timer file:

  borg-reminder.service
  borg-reminder.timer

Copy them to the user script folder:

  `~/.config/systemd/user`

To enable the service on startup:

  `systemctl --user enable borg-reminder.timer`

This will run the reminder script 5 minutes after the user login and once per day, if the user is logged in for a longer duration.

Further information about the systemd user services and timers on the arch wiki:

  https://wiki.archlinux.org/index.php/Systemd/User
  
  https://wiki.archlinux.org/index.php/Systemd/Timers
  

## System requirements

- borg (> 1.1)
- date
- systemd
- sh
- secret-tool
  
